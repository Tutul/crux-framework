/***********************************************************************************
 * MIT License                                                                     *
 *                                                                                 *
 * Copyright (c) 2022 Tutul                                                        *
 *                                                                                 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy    *
 * of this software and associated documentation files (the "Software"), to deal   *
 * in the Software without restriction, including without limitation the rights    *
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell       *
 * copies of the Software, and to permit persons to whom the Software is           *
 * furnished to do so, subject to the following conditions:                        *
 *                                                                                 *
 * The above copyright notice and this permission notice shall be included in all  *
 * copies or substantial portions of the Software.                                 *
 *                                                                                 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *
 * SOFTWARE.                                                                       *
 ***********************************************************************************/

#![no_std]
#![no_main]
#![feature(start)]
#![feature(custom_test_frameworks)]
#![reexport_test_harness_main = "test_main"]
#![test_runner(substance_framework::test_runner)]

#[macro_use]
extern crate substance_framework;
#[macro_use]
extern crate substance_macro;

#[no_mangle]
pub extern fn main(_argc: i32, _argv: *const *const u8) -> i32 {
    test_main();
    0
}

#[cfg(test)]
mod tests {
    #[substance_test(Should_panic)]
    fn test_equals() {
        sf_assert_eq!(0, 666);
    }

    #[substance_test(Should_panic)]
    fn test_non_equals() {
        sf_assert_ne!(0, 0);
    }

    #[substance_test(Should_panic)]
    fn test_lower() {
        sf_assert_lt!(0, -13);
    }

    #[substance_test(Should_panic)]
    fn test_lower_or_equals() {
        sf_assert_le!(0, -999);
    }

    #[substance_test(Should_panic)]
    fn test_greater() {
        sf_assert_gt!(7, 3654);
    }

    #[substance_test(Should_panic)]
    fn test_greater_or_equals() {
        sf_assert_ge!(-354, -1);
    }

    #[substance_test(Should_panic)]
    fn test_assert() {
        sf_assert!(false);
    }

    #[substance_test(Should_panic)]
    fn test_some() {
        let option : Option<bool> = None;
        sf_assert_some!(option);
    }

    #[substance_test(Should_panic)]
    fn test_ok() {
        let result : Result<bool, bool> = Err(false);
        sf_assert_ok!(result);
    }
}