/***********************************************************************************
 * MIT License                                                                     *
 *                                                                                 *
 * Copyright (c) 2022 Tutul                                                        *
 *                                                                                 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy    *
 * of this software and associated documentation files (the "Software"), to deal   *
 * in the Software without restriction, including without limitation the rights    *
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell       *
 * copies of the Software, and to permit persons to whom the Software is           *
 * furnished to do so, subject to the following conditions:                        *
 *                                                                                 *
 * The above copyright notice and this permission notice shall be included in all  *
 * copies or substantial portions of the Software.                                 *
 *                                                                                 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *
 * SOFTWARE.                                                                       *
 ***********************************************************************************/

#![feature(lang_items)]
#![feature(core_intrinsics)]
#![feature(panic_internals)]
#![feature(panic_info_message)]
#![feature(stmt_expr_attributes)]
#![no_std]

//////////////////////////////////////////////

use core::fmt::Arguments;
pub use core::intrinsics::abort;
pub use core::panic::{Location, PanicInfo};
use spin::Mutex;
use spin::RwLock;

//////////////////////////////////////////////

#[allow(unused_imports)]
use crate::output::combined::Combined;
#[allow(unused_imports)]
use crate::output::pretty::Pretty;
#[allow(unused_imports)]
use crate::output::report::Report;
use crate::output::Output;

//////////////////////////////////////////////

#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate libc_print;

#[doc(hidden)]
pub mod macros;

#[cfg(all(not(doc), not(test)))]
pub mod panic;

mod output;

//////////////////////////////////////////////

#[cfg(all(not(doc), not(test)))]
pub fn crash(file: &str, line: u32, column: u32, arguments: Option<&Arguments>) -> ! {
    panic::cx_panic(&PanicInfo::internal_constructor(
        arguments,
        &Location::internal_constructor(file, line, column),
        false,
    ))
}

#[cfg(any(doc, test))]
#[doc(hidden)]
pub fn crash(_: &str, _: u32, _: u32, _: Option<&Arguments>) -> ! {
    loop {}
}

//////////////////////////////////////////////

/// Represents the outcome of a particular test.
///
/// # Ignored
///
/// The test was ignored and didn't run.
///
/// # Passed
///
/// The test passed without any problem.
///
/// # Failed
///
/// The test didn't pass or an error occurred during its run.
/// If `msg` is provided, a custom message will be added to be shown on the screen.
/// You may find more details in `reason` if present.
///
/// # Aborted
///
/// The test was aborted for some reason.
/// This is usually a sign that the test isn't parsable by Substance.
/// You may find more details in `reason` if present.
///
/// # Skipped
///
/// The test wasn't able to run because the previous one was aborted.
#[derive(Clone, Copy)]
pub enum Outcome<'o> {
    Ignored,
    Passed,
    Failed { reason: Option<&'o str> },
    Aborted { reason: Option<&'o str> },
    Skipped,
}

//////////////////////////////////////////////

/// Struct that contain information about a test function to run.
///
/// - name: The name of the function to be printed.
/// - test_func: The function to call for the test, must be like _() {} (without return value).
/// - ignored: Indicates of the test should be ignored.
/// - should_panic: Indicates if the test should panic, if true, then a panicking test will be marked as success
pub struct TestBlock<'u> {
    pub name: &'u str,
    pub test_func: fn(),
    pub ignored: bool,
    pub should_panic: bool,
}

//////////////////////////////////////////////

/// Struct that represent the final result of the test run to display some number.
///
/// - has_aborted: Indicates whether a true panic occurred.
/// - num_ignored: The number of tests that were marked as non-running.
/// - num_passed: The number of tests that passed with success.
/// - num_failed; The number of tests that failed during they run.
/// - num_skipped: The number of tests that were skipped because a previous one aborted.
#[derive(Default)]
pub struct Conclusion {
    has_aborted: bool,
    num_ignored: usize,
    num_passed: usize,
    num_failed: usize,
    num_skipped: usize,
}

//////////////////////////////////////////////

/// Represents the current state of the framework.
///
/// Used to keep track of some global objects that may be required by the panic system (as it's static).
/// - panic: Indicates whether the current test triggered a (false) panic.
/// - msg: An optional message to display with the assertion or panic print.
/// - tests: The full array with all the tests, used also by panic to mark all aborted test as such.
/// - current: The current running unit test.
/// - printer: A Printer object so panic can still update the display with some messages and a result.
/// - conclusion: The optional conclusion that represents the final tests state, used for xUnit report.
/// - counter: The index of the current running unit test.
pub struct State<'s, T: Output> {
    pub panic: bool,
    pub msg: Option<&'s str>,
    printer: Mutex<T>,
    tests: Option<&'s [&'s TestBlock<'s>]>,
    current: Option<&'s TestBlock<'s>>,
    conclusion: Option<Conclusion>,
    counter: usize,
}

cfg_if::cfg_if! {
    if #[cfg(test)] {
        lazy_static! {
            pub static ref STATE: RwLock<State<'static, output::dummy::Dummy>> = spin::RwLock::new(State {
                panic: false,
                msg: None,
                printer: spin::Mutex::new(output::dummy::Dummy::new()),
                tests: None,
                current: None,
                conclusion: None,
                counter: 0
            });
        }
    } else if #[cfg(feature = "pretty")] {
        lazy_static! {
            pub static ref STATE: RwLock<State<'static, output::pretty::Pretty>> = spin::RwLock::new(State {
                panic: false,
                msg: None,
                printer: spin::Mutex::new(output::pretty::Pretty::new()),
                tests: None,
                current: None,
                conclusion: None,
                counter: 0
            });
        }
    } else if #[cfg(feature = "xml")] {
        lazy_static! {
            pub static ref STATE: RwLock<State<'static, output::report::Report>> = spin::RwLock::new(State {
                panic: false,
                msg: None,
                printer: spin::Mutex::new(output::report::Report::new()),
                tests: None,
                current: None,
                conclusion: None,
                counter: 0
            });
        }
    } else {
        lazy_static! {
            pub static ref STATE: RwLock<State<'static, output::combined::Combined>> = spin::RwLock::new(State {
                panic: false,
                msg: None,
                printer: spin::Mutex::new(output::combined::Combined::new()),
                tests: None,
                current: None,
                conclusion: None,
                counter: 0
            });
        }
    }
}

//////////////////////////////////////////////

pub fn test_runner(tests: &'static [&'static TestBlock<'static>]) {
    // Initialization
    {
        let mut state = STATE.write();
        state
            .printer
            .try_lock()
            .expect("Output lock is already locked")
            .init(tests);
        state.tests = Some(tests);
        state.conclusion = Some(Default::default());
    }

    // Print number of tests
    STATE.read().printer.lock().print_title();

    if tests.is_empty() {
        STATE.read().printer.lock().no_test();
        return;
    }

    tests.iter().for_each(|t| {
        {
            let mut state = STATE.write();
            let _ = state.current.insert(t);
            state.panic = false;
        }

        let test = STATE.read().current.unwrap();

        STATE.read().printer.lock().print_test(test.name);

        let outcome = if test.ignored {
            Outcome::Ignored
        } else {
            // Run the given function
            (test.test_func)();

            if STATE.read().panic == test.should_panic {
                Outcome::Passed
            } else {
                Outcome::Failed {
                    reason: STATE.write().msg.take(),
                }
            }
        };

        STATE.read().printer.lock().print_single_outcome(&outcome);

        {
            let mut state = STATE.write();
            match outcome {
                Outcome::Ignored => state.conclusion.as_mut().unwrap().num_ignored += 1,
                Outcome::Passed => state.conclusion.as_mut().unwrap().num_passed += 1,
                Outcome::Failed { .. } => state.conclusion.as_mut().unwrap().num_failed += 1,
                _ => state.conclusion.as_mut().unwrap().num_skipped -= 1,
            }
            state.current.take();
            state.counter += 1;
        }
    });

    STATE
        .read()
        .printer
        .lock()
        .print_summary(STATE.read().conclusion.as_ref().unwrap());
}

//////////////////////////////////////////////
/*
#[cfg(test)]
mod test {
    extern crate std;
    use super::*;

    fn dummy_test_fn() {}
    const DUMMY: TestBlock = TestBlock {
        name: "my dummy test",
        test_func: dummy_test_fn,
        ignored: false,
        should_panic: false
    };

    #[test]
    fn test_empty() {
        test_runner(&[]);
    }

    #[test]
    fn test_normal() {
        test_runner(&[&DUMMY]);
    }
}*/
