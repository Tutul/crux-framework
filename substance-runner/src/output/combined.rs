/***********************************************************************************
 * MIT License                                                                     *
 *                                                                                 *
 * Copyright (c) 2022 Tutul                                                        *
 *                                                                                 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy    *
 * of this software and associated documentation files (the "Software"), to deal   *
 * in the Software without restriction, including without limitation the rights    *
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell       *
 * copies of the Software, and to permit persons to whom the Software is           *
 * furnished to do so, subject to the following conditions:                        *
 *                                                                                 *
 * The above copyright notice and this permission notice shall be included in all  *
 * copies or substantial portions of the Software.                                 *
 *                                                                                 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *
 * SOFTWARE.                                                                       *
 ***********************************************************************************/

use crate::{Conclusion, Outcome, Output, Pretty, Report, TestBlock};

pub struct Combined {
    pretty: Pretty,
    report: Report,
}

impl Output for Combined {
    fn new() -> Self {
        Self {
            pretty: Pretty::new(),
            report: Report::new(),
        }
    }

    fn init(&mut self, tests: &[&TestBlock]) {
        self.pretty.init(tests);
        self.report.init(tests);
    }

    fn no_test(&self) {
        self.pretty.no_test();
        self.report.no_test();
    }

    fn print_title(&self) {
        self.pretty.print_title();
        self.report.print_title();
    }

    fn print_test(&self, name: &str) {
        self.pretty.print_test(name);
        self.report.print_test(name);
    }

    fn print_single_outcome(&self, outcome: &Outcome) {
        self.pretty.print_single_outcome(outcome);
        self.report.print_single_outcome(outcome);
    }

    fn print_summary(&self, conclusion: &Conclusion) {
        self.pretty.print_summary(conclusion);
        self.report.print_summary(conclusion);
    }
}
