/***********************************************************************************
 * MIT License                                                                     *
 *                                                                                 *
 * Copyright (c) 2022 Tutul                                                        *
 *                                                                                 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy    *
 * of this software and associated documentation files (the "Software"), to deal   *
 * in the Software without restriction, including without limitation the rights    *
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell       *
 * copies of the Software, and to permit persons to whom the Software is           *
 * furnished to do so, subject to the following conditions:                        *
 *                                                                                 *
 * The above copyright notice and this permission notice shall be included in all  *
 * copies or substantial portions of the Software.                                 *
 *                                                                                 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *
 * SOFTWARE.                                                                       *
 ***********************************************************************************/

use crate::output::Output;
use crate::{Conclusion, Outcome, TestBlock};

pub struct Report {
    total_tests: usize,
}

impl Output for Report {
    fn new() -> Self {
        Self { total_tests: 0 }
    }

    fn init(&mut self, tests: &[&TestBlock]) {
        self.total_tests = tests.len();
    }

    fn no_test(&self) {
        libc_eprintln!(
            "<?xml version='1.0' encoding='UTF-8'?>
<testsuite name='Substance Framework Testing' tests='0'>
</testsuite>"
        );
    }

    fn print_title(&self) {
        libc_eprintln!(
            "<?xml version='1.0' encoding='UTF-8'?>
<testsuite name='Substance Framework Testing' tests='{}'>",
            self.total_tests
        );
    }

    fn print_test(&self, name: &str) {
        libc_eprint!(
            "
    <testcase name='{}'>",
            name
        )
    }

    fn print_single_outcome(&self, outcome: &Outcome) {
        match outcome {
            Outcome::Passed => libc_eprint!(
                "
    </testcase>"
            ),
            Outcome::Failed { reason } => match reason {
                Some(err) => libc_eprint!(
                    "
        <error>
        {}
        </error>
    </testcase>",
                    err
                ),
                None => libc_eprint!(
                    "
        <error />
    </testcase>"
                ),
            },
            Outcome::Aborted { reason } => match reason {
                Some(err) => libc_eprint!(
                    "
        <failure message='{}'>
        {}
        </failure>
    </testcase>",
                    err,
                    err
                ),
                None => libc_eprint!(
                    "
        <failure />
    </testcase>",
                ),
            },
            Outcome::Ignored => libc_eprint!(
                "
        <ignored/>
    </testcase>"
            ),
            _ => libc_eprint!(
                "
        <skipped />
    </testcase>"
            ),
        }
    }

    fn print_summary(&self, _: &Conclusion) {
        libc_eprintln!(
            "
</testsuite>"
        );
    }
}
