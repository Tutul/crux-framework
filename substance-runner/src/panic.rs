/***********************************************************************************
 * MIT License                                                                     *
 *                                                                                 *
 * Copyright (c) 2022 Tutul                                                        *
 *                                                                                 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy    *
 * of this software and associated documentation files (the "Software"), to deal   *
 * in the Software without restriction, including without limitation the rights    *
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell       *
 * copies of the Software, and to permit persons to whom the Software is           *
 * furnished to do so, subject to the following conditions:                        *
 *                                                                                 *
 * The above copyright notice and this permission notice shall be included in all  *
 * copies or substantial portions of the Software.                                 *
 *                                                                                 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *
 * SOFTWARE.                                                                       *
 ***********************************************************************************/

pub use core::intrinsics::abort;
pub use core::panic::{Location, PanicInfo};

//////////////////////////////////////////////

use crate::{Conclusion, Outcome, Output, STATE};

//////////////////////////////////////////////

#[allow(clippy::missing_safety_doc)]
#[panic_handler]
#[no_mangle]
pub fn cx_panic(info: &PanicInfo) -> ! {
    let location = info.location();
    let message = info.message();

    let mut conclusion = Conclusion {
        has_aborted: true,
        num_ignored: usize::MAX,
        num_passed: 0,
        num_failed: usize::MAX,
        num_skipped: usize::MAX,
    };

    unsafe { STATE.force_write_unlock() }

    let state = STATE.read();

    unsafe { state.printer.force_unlock() };

    let printer = state.printer.lock();

    if state.conclusion.is_some() {
        conclusion.num_skipped = state.conclusion.as_ref().unwrap().num_skipped;
        conclusion.num_ignored = state.conclusion.as_ref().unwrap().num_ignored;
        conclusion.num_passed = state.conclusion.as_ref().unwrap().num_passed;
        conclusion.num_failed = state.conclusion.as_ref().unwrap().num_failed;
    }

    let mut counter = state.counter;

    if state.current.is_some() {
        let outcome = Outcome::Aborted {
            reason: match message {
                Some(msg) => msg.as_str(),
                None => None,
            },
        };
        conclusion.num_skipped -= 1;
        counter += 1;
        printer.print_single_outcome(&outcome);
    }

    let outcome = Outcome::Skipped;
    for index in counter..state.tests.unwrap().len() {
        printer.print_test(state.tests.unwrap()[index].name);
        printer.print_single_outcome(&outcome);
    }

    printer.print_summary(&conclusion);

    libc_println!();

    if message.is_some() && location.is_some() {
        libc_println!(
            "[PANIC] {}:{}:{}",
            location.unwrap().file(),
            location.unwrap().line(),
            location.unwrap().column()
        );
        libc_println!("{}", message.unwrap());
    } else if message.is_some() {
        libc_println!("[PANIC] {} (unknown location)", message.unwrap());
    } else if location.is_some() {
        libc_println!(
            "[PANIC] A panic occurred at {}:{}:{}",
            location.unwrap().file(),
            location.unwrap().line(),
            location.unwrap().column()
        );
    } else {
        libc_println!("[PANIC] A panic occurred but no information is available");
        libc_println!("    >>> {:?}", info);
    }
    libc_println!();

    abort()
}

#[allow(clippy::missing_safety_doc)]
#[lang = "eh_personality"]
#[no_mangle]
pub fn rust_eh_personality() -> ! {
    unreachable!("eh_personality")
}

#[allow(clippy::missing_safety_doc)]
#[allow(non_snake_case)]
#[no_mangle]
pub fn _Unwind_Resume() -> ! {
    unreachable!("_Unwind_Resume")
}
