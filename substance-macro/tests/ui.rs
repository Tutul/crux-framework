/***********************************************************************************
 * MIT License                                                                     *
 *                                                                                 *
 * Copyright (c) 2022 Tutul                                                        *
 *                                                                                 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy    *
 * of this software and associated documentation files (the "Software"), to deal   *
 * in the Software without restriction, including without limitation the rights    *
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell       *
 * copies of the Software, and to permit persons to whom the Software is           *
 * furnished to do so, subject to the following conditions:                        *
 *                                                                                 *
 * The above copyright notice and this permission notice shall be included in all  *
 * copies or substantial portions of the Software.                                 *
 *                                                                                 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *
 * SOFTWARE.                                                                       *
 ***********************************************************************************/

#[test]
fn setup() {
    let t = trybuild::TestCases::new();
    t.pass("tests/ui/01-simple.rs");
    t.pass("tests/ui/02-ignore.rs");
    t.pass("tests/ui/03-should-panic.rs");
    t.pass("tests/ui/04-full.rs");
    t.compile_fail("tests/ui/05-missing-features.rs");
}

#[test]
fn sanitize_lit_args() {
    let t = trybuild::TestCases::new();
    t.compile_fail("tests/ui/06-lit-arg.rs");
}

#[test]
fn sanitize_meta_args() {
    let t = trybuild::TestCases::new();
    t.compile_fail("tests/ui/07-metaList-arg.rs");
    t.compile_fail("tests/ui/08-metaNamed-arg.rs");
    t.compile_fail("tests/ui/09-metaPath-arg.rs");
}
