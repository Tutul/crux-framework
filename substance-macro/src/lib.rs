/***********************************************************************************
 * MIT License                                                                     *
 *                                                                                 *
 * Copyright (c) 2022 Tutul                                                        *
 *                                                                                 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy    *
 * of this software and associated documentation files (the "Software"), to deal   *
 * in the Software without restriction, including without limitation the rights    *
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell       *
 * copies of the Software, and to permit persons to whom the Software is           *
 * furnished to do so, subject to the following conditions:                        *
 *                                                                                 *
 * The above copyright notice and this permission notice shall be included in all  *
 * copies or substantial portions of the Software.                                 *
 *                                                                                 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *
 * SOFTWARE.                                                                       *
 ***********************************************************************************/

#![no_std]

//////////////////////////////////////////////

extern crate alloc;

use alloc::format;
use alloc::string::ToString;
use proc_macro::TokenStream;
use proc_macro2::Span;
use quote::quote;
use syn::{parse_macro_input, AttributeArgs, Ident, ItemFn, Meta, NestedMeta};

//////////////////////////////////////////////

/// Define a unit test that should run with Substance.
///
/// The function with this attribute will be marked as a Substance unit test function.
/// The current macro notation will be replaced by the appropriate object for Substance and marked
/// for cargo to see it as a test to be provided to the runner.
///
/// Note that the testing function should respect one of this signature:
///   - `fn _() {}` for basic test
//   - `fn _(...) {}` for test using Theory
//   - `fn _() -> Result<T, E> {}` for test that will combine assertions
//   - `fn _(...) -> Result<T, E> {}` for test that use Theory with combine assertions
/// The Framework won't be able to ensure that you wrote your test correctly and will try to do what
/// you ask it to do.
///
/// The macro supports two arguments (for now) that enable you to specify if a test should be ignored
/// or if a test is expected to fail.
///
/// Don't forget to enable custom test framework and specify the test runner using these two macro:
/// ```rust
/// #![feature(custom_test_frameworks)]
/// #![test_runner(substance_framework::test_runner)]
/// ```
///
/// # Examples
///
/// To define a test function:
///
/// ```rust
/// #[substance_test]
/// fn my_test() {
///     [...]
/// }
/// ```
///
/// To define a test function that should panic:
///
/// ```rust
/// #[substance_test(Should_panic)]
/// fn my_test_that_must_panic() {
///     [...]
/// }
/// ```
///
/// To define a test function that should to be ignored:
///
/// ```rust
/// #[substance_test(Ignore)]
/// fn my_ignored_test() {
///     [...]
/// }
/// ```
///
/// Both can be combined (order doesn't matter):
///
/// ```rust
/// #[substance_test(Ignore, Should_panic)]
/// fn my_ignored_panicking_test() {
///     [...]
/// }
/// ```
#[proc_macro_attribute]
pub fn substance_test(args: TokenStream, input: TokenStream) -> TokenStream {
    let attr_args = parse_macro_input!(args as AttributeArgs);
    let item_fn = parse_macro_input!(input as ItemFn);
    let ident = item_fn.sig.ident.to_string();

    let test_name = ident.as_str();
    let test_func = item_fn.block;
    let test_ident = Ident::new(
        format!("__{}_SUBSTANCE_TEST_BLOCK", ident.to_uppercase()).as_str(),
        Span::call_site(),
    );

    let mut ignore = false;
    let mut should_panic = false;

    attr_args.iter().for_each(|arg| match arg {
        NestedMeta::Meta(m) => match m {
            Meta::Path(p) => {
                let ident = p.get_ident().expect("Cannot read Meta Path");
                if ident.eq("Ignore") {
                    ignore = true;
                } else if ident.eq("Should_panic") {
                    should_panic = true;
                } else {
                    unreachable!("Unsupported attribute of type Meta for `#[substance_test]`");
                }
            }
            _ => {
                unreachable!("Unsupported attribute of type Meta for `#[substance_test]`");
            }
        },
        NestedMeta::Lit(_) => {
            unreachable!("Unsupported attribute of type Lit for `#[substance_test]`");
        }
    });

    quote!(
        #[test_case]
        const #test_ident: substance_framework::TestBlock = substance_framework::TestBlock {
            name: #test_name,
            test_func: || #test_func,
            ignored: #ignore,
            should_panic: #should_panic,
        };
    )
    .into()
}