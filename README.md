# Substance Framework

Substance Framework is a `[no_std]` testing framework for rust and cargo that can work even with custom target.
It enabling you to perform units testing on `Core`-only project (without the need to build `sdt` for the target).
The framework relies on some nightly features to provide a complete system that can handle most of the hard work for you.

Any project that require a target without `std` support (minimalistic target or custom system) can use this project to
avoid the need to reimplement a test runner as it is described in the unstable book.

This project is under the [![](https://img.shields.io/badge/license-MIT-green.svg)](https://opensource.org/licenses/MIT)

## Features

- Provide a working `no_std` runner for unit testing
- Display a nice interface with the result
- handle true panic and abort all the tests
- provide a simple macro to put on your testing function
- provide a set of assertion that replace the default one

## Sub-projects

- [substance-macro](https://gitlab.com/Tutul/substance-framework/-/tree/dev/substance-macro) (mandatory): provide all required macro work with the framework [![](https://img.shields.io/crates/v/substance-macro)](https://crates.io/crates/substance-macro/) [![](https://img.shields.io/docsrs/substance-macro?label=docs%20macro)](https://docs.rs/substance-macro/latest/substance_macro/)
- [substance-runner](https://gitlab.com/Tutul/substance-framework/-/tree/dev/substance-runner) (mandatory): provide the main test runner [![](https://img.shields.io/crates/v/substance-framework)](https://crates.io/crates/substance-framework) [![](https://img.shields.io/docsrs/substance-framework?label=docs%20framework)](https://docs.rs/substance-framework/latest/substance_framework/)

## A short introduction

```rust

#![feature(custom_test_frameworks)]
#![test_runner(substance_framework::test_runner)]

#[macro_use]
extern crate substance_framework;
#[macro_use]
extern crate substance_macro;

#[cfg(test)]
mod tests {
    #[substance_test]
    fn test_equals() {
        sf_assert_eq!(0, 0);
    }

    #[substance_test]
    fn test_non_equals() {
        sf_assert_ne!(0, -1);
    }

    #[substance_test]
    fn test_lower() {
        sf_assert_lt!(0, 1);
    }

    #[substance_test]
    fn test_lower_or_equals() {
        sf_assert_le!(0, 0);
    }

    #[substance_test]
    fn test_greater() {
        sf_assert_gt!(7, -1);
    }

    #[substance_test]
    fn test_greater_or_equals() {
        sf_assert_ge!(0, -1);
    }

    #[substance_test]
    fn test_is_true() {
        sf_assert!(53 != 54);
    }

    #[substance_test]
    fn test_failure() {
        sf_assert!(53 == 54);
    }

    #[substance_test]
    fn test_failure_with_msg() {
        sf_assert!(53 == 54, "I'm a bad little test");
    }

    #[substance_test("Should_panic")]
    fn test_should_fail() {
        sf_assert!(false);
    }

    #[substance_test("Ignore")]
    fn test_is_true_to_ignore() {
        sf_assert!(false, "shouldn't be visible");
    }

    #[substance_test("Should_panic")]
    fn test_panic() {
        panic!();
    }

    #[substance_test("Should_panic")]
    fn test_panic_with_message() {
        panic!("YEAAAAAA");
    }

    #[substance_test("Should_panic")]
    fn test_panic_with_arguments() {
        panic!("{}{}{}", "Y", "E", "AAAAAA");
    }

    #[substance_test("Should_panic", "Ignore")]
    fn test_panic_ignored() {
        panic!();
    }
}
```

## Minimum supported Rust version

You must use at least Rust nightly-2022-02-12 because the crate depend on some unstable features.
Those are required to provide the test runner and manage a panic handler that can manage most case safely.

## Roadmap

- [x] Provide macro to ease the usage of this library => first iteration

- [x] Provide macro replacement for assertion (default one use panic implementation directly)

- [x] Get a UI upgrade => using the `libc` crate to access STDOUT

- [x] Support JUnit-like XML report generation => basic system

- [ ] Provide Setup() and TearDown()

- [ ] Implement a full XUnit system

- [ ] Provide a comprehensive declarative system to construct any assertion

- [ ] Find a way to optimize the run time (`core` doesn't provide thread support)

- [ ] etc.


